﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myTunes.Models
{
    public class TopGenre
    {
        public string Genre { get; set; }
        public int Total { get; set; }
    }
}
