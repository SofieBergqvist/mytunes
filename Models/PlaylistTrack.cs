﻿using System.ComponentModel.DataAnnotations;

namespace myTunes.Models
{
    public class PlaylistTrack
    {
        [Required]
        public int PlaylistId { get; set; }

        [Required]
        public int TrackId { get; set; }
    }
}
