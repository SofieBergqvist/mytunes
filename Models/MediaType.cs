﻿using System.ComponentModel.DataAnnotations;

namespace myTunes.Models
{
    public class MediaType
    {
        [Required]
        public int MediaTypeId { get; set; }

        [StringLength(120)]
        public string Name { get; set; }

    }
}
