﻿using System.ComponentModel.DataAnnotations;

namespace myTunes.Models
{
    public class Genre
    {
        [Required]
        public int GenreId { get; set; }

        [StringLength(120)]
        public string Name { get; set; }

    }
}
