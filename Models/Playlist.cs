﻿using System.ComponentModel.DataAnnotations;

namespace myTunes.Models
{
    public class Playlist
    {
        [Required]
        public int PlaylistId { get; set; }

        [StringLength(120)]
        public string Name { get; set; }

    }
}
