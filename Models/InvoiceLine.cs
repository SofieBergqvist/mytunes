﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace myTunes.Models
{
    public class InvoiceLine
    {
        [Required]
        public int InvoiceLineId { get; set; }

        [Required]
        public int InvoiceId { get; set; }

        [Required]
        public int TrackId { get; set; }

        [Required]
        [Range(0, 99999999.99)]
        public decimal UnitPrice { get; set; }

        [Required]
        public int Quantity { get; set; }

    }
}
