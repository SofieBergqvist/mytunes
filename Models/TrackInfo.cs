﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myTunes.Models
{
    public class TrackInfo
    {
        public string TrackName { get; set; }
        public string ArtistName { get; set; }
        public string AlbumTitle { get; set; }
        public string GenreName { get; set; }
    }
}
