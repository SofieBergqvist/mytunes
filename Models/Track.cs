﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace myTunes.Models
{
    public class Track
    {
        [Required]
        public int TrackId { get; set; }

        [StringLength(200)]
        public string Name { get; set; }

        public int AlbumId { get; set; }

        public int MediaTypeId { get; set; }

        public int GenreId { get; set; }

        [StringLength(220)]
        public string Composer { get; set; }

        public int Milliseconds { get; set; }

        public int Bytes { get; set; }

        [Required]
        [Range(0.00, 99.99)]
        public decimal UnitPrice { get; set; }

    }
}
