﻿using System.ComponentModel.DataAnnotations;

namespace myTunes.Models
{
    public class Artist
    {
        [Required]
        public int ArtistId { get; set; }

        [StringLength(120)]
        public string Name { get; set; }

    }
}
