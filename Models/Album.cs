﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace myTunes.Models
{
    public class Album
    {
        [Required]
        public int AlbumId { get; set; }

        [Required]
        [StringLength(160)]
        public string Title { get; set; }

        [Required]
        public int ArtistId { get; set; }

    }
}
