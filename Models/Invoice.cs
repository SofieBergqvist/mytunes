﻿using System;
using System.ComponentModel.DataAnnotations;

namespace myTunes.Models
{
    public class Invoice
    {
        [Required]
        public int InvoiceId { get; set; }

        [Required]
        public int CustomerId { get; set; }

        [Required]
        public DateTime InvoiceDate { get; set; }

        [StringLength(70)]
        public string BillingAddress { get; set; }

        [StringLength(40)]
        public string BillingCity { get; set; }

        [StringLength(40)]
        public string BillingState { get; set; }

        [StringLength(40)]
        public string BillingCountry { get; set; }

        [StringLength(10)]
        public string BillingPostalCode { get; set; }

        [Range(0.00, 99999999.99)]
        public decimal Total { get; set; }

    }
}
