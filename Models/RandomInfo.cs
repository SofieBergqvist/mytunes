﻿using System.Collections.Generic;

namespace myTunes.Models
{
    /// <summary>
    /// viewModel show to user, this is not domain model. 
    /// </summary>
    public class RandomInfo
    {
        public List<Artist> Artists { get; set; }
        public List<Track> Tracks { get; set; }
        public List<Genre> Genres { get; set; }

        public RandomInfo()
        {
            Artists = new List<Artist>();
            Tracks = new List<Track>();
            Genres = new List<Genre>();
        }
    }
}
