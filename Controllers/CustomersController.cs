﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using myTunes.Models;
using myTunes.Repository;


namespace myTunes.Controllers
{
    /// <summary>
    /// Set route api/customers 
    /// Other methods in class will extend on this endpoint
    /// </summary>
    [Route("api/customers")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly CustomersRepository _customersRepository;

        public CustomersController(CustomersRepository customersRepository)
        {
            _customersRepository = customersRepository;
        }

        /// <summary>
        /// GET: api/customers
        /// </summary>
        /// <returns>Returns a JSON object with all Customers in the database</returns>
        [HttpGet]
        public ActionResult<IEnumerable<Customer>> Get()
        {
            //Fetch all Customers
            return Ok(_customersRepository.GetCustomers());
        }

        /// <summary>
        /// POST: api/customers
        /// </summary>
        /// <param Customer="customer">Customer object to be added</param>
        /// <returns>Returns a new customer object assigned with the next id number in the database </returns>
        [HttpPost]
        public ActionResult Post(Customer customer)
        {
            //Add new Customer 
            bool success = _customersRepository.AddNewCustomer(customer);

            //Server failed to fullfill the request
            if (!success)
            {
                return StatusCode(500);
            }

            return CreatedAtAction(nameof(Get), new { id = customer.CustomerId }, success);
        }

        /// <summary>
        /// PATCH: api/customers/4
        /// Updates an existing customer in the database based on its CustomerID 
        /// PATCH instead of PUT will keep old data and update new data
        /// </summary>
        /// <param Customer="customer">Customer object to update</param>
        /// <param int="customerId">specified customer ID </param>
        /// <returns>Returns updated customer object if sucessful</returns>
        //PATCH api/customers/4
        [HttpPatch("{customerId}")]
        public ActionResult Patch(Customer customer, int customerId)
        {
            //if (customerId != customer.CustomerId)
            //{
            //    return BadRequest();
            //}

            //Update an existing Customer in Repository by given id
            bool success = _customersRepository.UpdateCustomer(customer, customerId);

            //Server failed to fullfill the request
            if (!success)
            {
                return StatusCode(500);
            }

            return NoContent();
        }


        /// <summary>
        /// GET: api/customers/country
        /// </summary>
        /// <param int="customerId">specified customer ID</param>
        /// <returns>Returns a JSON Object with the number of customers in each country in descending order</returns>
        [HttpGet("country")]
        public ActionResult<IEnumerable<CountryCustomer>> GetCountries(int customerId)
        {
            return Ok(_customersRepository.GetCustomersInCountries());
        }

        /// <summary>
        /// GET: api/top/genre/4
        /// </summary>
        /// <param int="customerId">specified customer ID</param>
        /// <returns>Returns a JSON Object with a customers favourite genre and the total issued invoices for this genre</returns>
        [HttpGet("genre/top/{customerId}")]
        public ActionResult<IEnumerable<TopGenre>> TopGenre(int customerId)
        {
            return Ok(_customersRepository.GetTopGenre(customerId));
        }


        [HttpGet("invoice/all/amount/desc")]
        public ActionResult<IEnumerable<CustomerSpending>> TopSpenders()
        {
            //Fetch all Customers
            return Ok(_customersRepository.GetHighestSpender());
        }




    }
}

