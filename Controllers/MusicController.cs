﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using myTunes.Models;
using myTunes.Repository;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace myTunes.Controllers
{
    /// <summary>
    /// Set route api/music 
    /// Other methods in class will extend on this endpoint
    /// </summary>
    [Route("api/music")]
    [ApiController]
    public class MusicController : ControllerBase
    {
        private readonly MusicRepository _musicRepository;

        public MusicController(MusicRepository musicRepository)
        {
            _musicRepository = musicRepository;
        }

        /// <summary>
        ///  GET: api/music
        /// </summary>
        /// <returns>Fetch 5 random tracks, genres and artist</returns>
        [HttpGet]
        public ActionResult<IEnumerable<RandomInfo>> Get()
        {
            return Ok(_musicRepository.GetRandomInfo());
        }


        /// <summary>
        /// GET api/music/search/?track={value}
        /// </summary>
        /// <param string="track">specified track from query </param>
        /// <returns></returns>
        [HttpGet("search")]
        public ActionResult<IEnumerable<TrackInfo>> Get([FromQuery] string track)
        {
            return Ok(_musicRepository.GetTrack(track));
        }
    }
}
