﻿using System.Data.SqlClient;

namespace myTunes.Repository
{
    public class ConnectionHelper
    {
        /// <summary>
        ///Build a connection string
        ///DataSorce: Servername
        ///InitialCatalog: Name of Database
        /// </summary>
        public static string GetConnectionstring()
        {
            SqlConnectionStringBuilder connectionBuilder = new SqlConnectionStringBuilder
            {
                DataSource = "DESKTOP-A1MBUSS\\SQLEXPRESS",
                InitialCatalog = "Chinook",
                IntegratedSecurity = true
            };
            return connectionBuilder.ConnectionString;
        }

      }
}
