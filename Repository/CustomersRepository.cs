﻿using myTunes.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace myTunes.Repository
{
    public class CustomersRepository
    {
        /// <summary>
        /// Method to get all Customers in the database 
        /// </summary>
        /// <returns>Returns a list of all all the customers</returns>
        public List<Customer> GetCustomers()
        {
            //Handle Response
            List<Customer> customerList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";

            try
            {
                using SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring());
                conn.Open();
                //Issuing sql command 
                using SqlCommand cmd = new SqlCommand(sql, conn);
                //Executing command 
                using SqlDataReader reader = cmd.ExecuteReader();
                //result set contained in reader object
                while (reader.Read())
                {
                    //create customer object and assign return values
                    Customer customer = new Customer
                    {
                        CustomerId = reader.GetInt32(0),
                        FirstName = reader.GetString(1),
                        LastName = reader.GetString(2),
                        Country = reader.GetString(3),
                        //Return based on null values / existing values 
                        PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4),
                        Phone = reader.IsDBNull(5) ? null : reader.GetString(5),
                        Email = reader.GetString(6)
                    };
                    customerList.Add(customer);
                }
            }
            //Exception Handeling
            catch (SqlException ex)
            {

                Console.WriteLine(ex);
            }

            return customerList;
        }

        /// <summary>
        /// Method to create and add new customer to ur database
        /// </summary>
        /// <param Customer="customer">Customer object to be added</param>
        /// <returns></returns>
        public bool AddNewCustomer(Customer customer)
        {
            if (customer == null) return false;
            bool success = false;

            string sql = "INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES (@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";

            try
            {
                using SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring());
                conn.Open();
                using SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                cmd.Parameters.AddWithValue("@Country", customer.Country);
                cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                cmd.Parameters.AddWithValue("@Email", customer.Email);
                success = cmd.ExecuteNonQuery() > 0;
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return success;
        }
            

        /// <summary>
        /// Method to update information about a specific customer
        /// </summary>
        /// <param Customer="customer">Customer object to update</param>
        /// <param int="customerId">Used to specify customer ID</param>
        /// <returns></returns>
        public bool UpdateCustomer(Customer customer, int customerId)
        {
            if (customer == null) return false;

            bool success = false;

            // sql: if == null, then keep the value
            string sql = "UPDATE Customer " +
                         "SET FirstName = ISNULL(@FirstName, FirstName), " +
                         "LastName = ISNULL(@LastName, LastName), " +
                         "Company = ISNULL(@Company, Company), " +
                         "Address = ISNULL(@Address, Address), " +
                         "City = ISNULL(@City, City), " +
                         "State = ISNULL(@State, State), " +
                         "Country = ISNULL(@Country, Country), " +
                         "PostalCode = ISNULL(@PostalCode, PostalCode), " +
                         "Phone = ISNULL(@Phone, Phone), " +
                         "Fax = ISNULL(@Fax, Fax), " +
                         "Email = ISNULL(@Email, Email) " +
                         "WHERE CustomerId = @CustomerId";

            try
            {
                using SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring());
                conn.Open();
                using SqlCommand cmd = new SqlCommand(sql, conn);
                // if the value in object is null then convert it to DBNull so that the sql statement can execute it
                cmd.Parameters.AddWithValue("@CustomerId", customerId);
                cmd.Parameters.AddWithValue("@FirstName", customer.FirstName ?? Convert.DBNull);
                cmd.Parameters.AddWithValue("@LastName", customer.LastName ?? Convert.DBNull);
                cmd.Parameters.AddWithValue("@Company", customer.Company ?? Convert.DBNull);
                cmd.Parameters.AddWithValue("@Address", customer.Address ?? Convert.DBNull);
                cmd.Parameters.AddWithValue("@City", customer.City ?? Convert.DBNull);
                cmd.Parameters.AddWithValue("@State", customer.State ?? Convert.DBNull);
                cmd.Parameters.AddWithValue("@Country", customer.Country ?? Convert.DBNull);
                cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode ?? Convert.DBNull);
                cmd.Parameters.AddWithValue("@Phone", customer.Phone ?? Convert.DBNull);
                cmd.Parameters.AddWithValue("@Fax", customer.Fax ?? Convert.DBNull);
                cmd.Parameters.AddWithValue("@Email", customer.Email ?? Convert.DBNull);
                //Execute non-query, notify if anything goes wrong 
                success = cmd.ExecuteNonQuery() > 0;
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
                
            }
            return success;
        }

        /// <summary>
        /// Method to get all customers for each country 
        /// </summary>
        /// <returns>Returns a list with the number of customers for each country in descending order</returns>
        public List<CountryCustomer> GetCustomersInCountries()
        {
            //Handle Response
            List<CountryCustomer> customerCountryList = new List<CountryCustomer>();
            string sql = "SELECT Customer.Country, COUNT(Customer.Country) AS Customers FROM Customer " +
                         "GROUP BY Country " +
                         "ORDER BY Customers DESC";

            try
            {
                using SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring());
                conn.Open();
                //Issuing sql command 
                using SqlCommand cmd = new SqlCommand(sql, conn);
                //Executing command 
                using SqlDataReader reader = cmd.ExecuteReader();
                //result set contained in reader object
                while (reader.Read())
                {
                    //create countryCustomer object and assign return values
                    CountryCustomer countryCustomer = new CountryCustomer
                    {
                        Country = reader.GetString(0),
                        Customer = reader.GetInt32(1),
                    };
                    customerCountryList.Add(countryCustomer);
                }
            }
            //Exception handeling
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
                return null;
            }

            return customerCountryList;
        }

     
        /// <summary>
        /// Return the most popular genre for a given customer, specified by customerID
        /// </summary>
        /// <param int="customerId">Used to specify customer ID</param>
        /// <returns>Returns a list with the customers favourite genre counted by issued invoices </returns>
        public List<TopGenre> GetTopGenre(int customerId)
        {
            List<TopGenre> topGenres = new List<TopGenre>();



            string sql = "SELECT Genre.Name, Count(Genre.Name) as Total FROM Customer " +
                         "INNER JOIN Invoice on Invoice.CustomerId = @CustomerId " +
                         "INNER JOIN InvoiceLine on InvoiceLine.InvoiceId = Invoice.InvoiceId, " +
                         "Genre, Track WHERE Invoice.CustomerId = Customer.CustomerId " +
                         "AND InvoiceLine.TrackId = Track.TrackId " +
                         "AND Track.GenreId = Genre.GenreId " +
                         "GROUP BY Genre.Name " +
                         "ORDER BY COUNT(Genre.Name) DESC";

            try
            {
                using SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring());
                conn.Open();
                using SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@CustomerId", customerId);
                using SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    if (topGenres.Any())
                    {
                        if (reader.GetInt32(1) < topGenres[0].Total)
                            break;
                    }
                    TopGenre topGenre = new TopGenre()
                    {
                        Genre = reader.GetString(0),
                        Total = reader.GetInt32(1)
                    };
                    topGenres.Add(topGenre);
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
                return null;
            }

            return topGenres;
        }


        public List<CustomerSpending> GetHighestSpender()
        {

            //Handle Response
            List<CustomerSpending> customerSpendings = new List<CustomerSpending>();
            string sql = "SELECT Customer.CustomerId, FirstName, LastName, SUM(Total) AS Total FROM Invoice, Customer " +
                         "WHERE Customer.CustomerId = Invoice.CustomerId " +
                         "GROUP BY Customer.CustomerId, FirstName, LastName " +
                         "ORDER BY Total DESC;";

            try
            {
                using SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring());
                conn.Open();
                //Issuing sql command 
                using SqlCommand cmd = new SqlCommand(sql, conn);
                //Executing command 
                using SqlDataReader reader = cmd.ExecuteReader();
                //result set contained in reader object
                while (reader.Read())
                {
                    //create customerSpending object and assign return values to list
                    CustomerSpending customerSpending = new CustomerSpending
                    {
                        CustomerId = reader.GetInt32(0),
                        FirstName = reader.GetString(1),
                        LastName = reader.GetString(2),
                        TotalSpending = reader.GetDecimal(3)
                    };
                    customerSpendings.Add(customerSpending);
                }
            }
            catch (SqlException ex)
            {

                //Find a way to Handle Error - common response object?
                Console.WriteLine(ex);
            }

            return customerSpendings;
        }


    }
}
