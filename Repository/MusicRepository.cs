﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using myTunes.Models;

namespace myTunes.Repository
{
    public class MusicRepository
    {
        /// <summary>
        /// Method to get top 5 artists, tracks and genres from database
        /// </summary>
        /// <returns>Returns a list with 5 random artists, tracks and genre</returns>
        public RandomInfo GetRandomInfo()
        {
            RandomInfo randomInfos = new RandomInfo();

            string sqlArtist = "SELECT TOP 5 * FROM Artist ORDER BY NEWID()";
            string sqlTrack = "SELECT TOP 5 * FROM Track ORDER BY NEWID()";
            string sqlGenre = "SELECT TOP 5 * FROM Genre ORDER BY NEWID()";

            try
            {
                using SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring());
                conn.Open();
                //Issuing sqlArtist command 
                using (SqlCommand cmd = new SqlCommand(sqlArtist, conn))
                {
                    //Executing command 
                    //result set contained in reader object
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            //create artist object and assign return values
                            Artist artist = new Artist()
                            {
                                ArtistId = reader.GetInt32(0),
                                Name = reader.GetString(1)
                            };
                            randomInfos.Artists.Add(artist);
                        }
                    }
                }

                //Prepare SqlCommand 
                using (SqlCommand cmd = new SqlCommand(sqlTrack, conn))
                {
                    using SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Track track = new Track()
                        {
                            TrackId = reader.GetInt32(0),
                            Name = reader.GetString(1),
                            AlbumId = reader.GetInt32(2),
                            MediaTypeId = reader.GetInt32(3),
                            GenreId = reader.GetInt32(4),
                            Milliseconds = reader.GetInt32(6),
                            Bytes = reader.GetInt32(7),
                            UnitPrice = reader.GetDecimal(8)
                        };
                        randomInfos.Tracks.Add(track);
                    }
                }
                //Prepare SqlCommand 
                using (SqlCommand cmd = new SqlCommand(sqlGenre, conn))
                {
                    using SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Genre genre = new Genre()
                        {
                            GenreId = reader.GetInt32(0),
                            Name = reader.GetString(1),
                        };
                        randomInfos.Genres.Add(genre);
                    }
                }
            }

            catch (SqlException ex)
            {
                Console.WriteLine(ex);
                return randomInfos;
            }

            return randomInfos;
        }

        /// <summary>
        ///  Method to get track name, artist name, album title and genre from a user query on track name
        /// </summary>
        /// <param name="trackName"></param>
        /// <returns>Returns a list with tracks that match user query </returns>
        public List<TrackInfo> GetTrack(string trackName)
        {
            List<TrackInfo> trackInfos = new List<TrackInfo>();



            string sql = "SELECT Track.Name, Artist.Name, Album.Title, Genre.Name From Track, Album, Artist, Genre " +
                         "WHERE Track.Name LIKE '%' + @TrackName + '%' AND " +
                         "Track.AlbumId = Album.AlbumId AND Album.ArtistId = Artist.ArtistId AND Track.GenreId = GENRE.GenreId";
            //Try to connect
            try
            {
                //Get connection string from connection builder 
                using SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring());
                conn.Open();
                //If connection is open: 
                Console.WriteLine("Connection sucesfull");

                //Prepare Command after we are connected
                using SqlCommand cmd = new SqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("@TrackName", trackName);
                using SqlDataReader reader = cmd.ExecuteReader();
                
                while (reader.Read())
                {
                    TrackInfo trackInfo = new TrackInfo()
                    {
                        TrackName = reader.GetString(0),
                        ArtistName = reader.GetString(1),
                        AlbumTitle = reader.GetString(2),
                        GenreName = reader.GetString(3)
                    };
                    trackInfos.Add(trackInfo);
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
                return null;
            }

            return trackInfos;
        }
    }
}
