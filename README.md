# MyTunes
My Tunes is a working prototype of a REST API buildt in ASP.NET Core that performs CRUD (Create, Read, Update, Delete) operations of the Chinooke Database. 

## Chinook
Chinook is a sample database that simulates an online music store including tables for artists, albums, media tracks, invoices and customers.

## Postman
Postman is an API client that can be used to verify our API. 
A collection with requests to the API-endpoints can be found in the Postman Directory.  

# API Endpoints

## /api/music 
GET Method for fetching 5 random artists, songs and genres. 

## /api/music/search/?track=(value)  
GET Method that returns a list with tracks that match the user query(value). Will also display album title, artist and genre.  

## /api/customers 
GET Method for fetching all customers from the database and display their Customer ID, First name, Last name, Country, Postal Code, Phone number and Email. 

## /api/customers/
POST Method for adding a new customer to the database with the same attributes listed for the GET method above. 

## /api/customers/{customerId}
PATCH Method for updating an existing customer to the database. {customerID} should be replaced with an integer in Postman.

## /api/customers/country
GET Method for fetching customers by country 

## /api/customers/genre/top/{customerId}
GET Method that returns a JSON Object with a customers favourite genre and the total issued invoices for this genre

## /api/customers/invoice/all/amount/desc
GET Method that returns a list of the highest spending customers displayed in descending order based on theid invoice table.


## Getting Started
* Download or Clone to a local directory
* Open Chinook_SqlServer_AutoIncrementPKs.sql inside Chinhook DB folder with MSSMS and connect
* Copy the Server name "[Device Name]/SQLEXPRESS"
* Open solution in Visual Studio
* Change DataSource value in myTunes/Repository/ConnectionHelper.cs to the copied Server name
* Build solution then run IIS Express to start API
    * Browser will open and you can enter GET endpoints specified above
* In Postman:
    * Import "Customer.postman_collection.json" and Music.postman_collection.json from myTunes/Postman collections
    * Upon first request, disable ssl certifications when asked at the bottom
    * Pick an HTTP method from the lefthand meny and click on the "Send" button to see the response. 
        * Make sure to have the App running from visual studio

### Contributors
Bao Nguyen <br/>
Sofie Bergqvist
